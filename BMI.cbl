       IDENTIFICATION DIVISION. 
       PROGRAM-ID. BMI.
       AUTHOR. WORAMET.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01  WEIGHT      PIC 9(3)V99 VALUE ZEROS.
       01  HEIGHT      PIC 9(3)V99 VALUE ZEROS.
       01  BMI         PIC 9(2)V99 VALUE ZEROS.
       01  BMI-DETAIL  PIC X(50).
           88 LESS-WEIGHT       VALUE "Underweight".
           88 NORMAL-WEIGHT     VALUE "Normal (Healthy)".
           88 BUXOM-WEIGHT      VALUE "Overweight".
           88 FAT-WEIGHT        VALUE "Fat".
           88 VERY-FAT-WEIGHT   VALUE "Very Fat".

       PROCEDURE DIVISION.
       BEGIN.
           DISPLAY   "Enter weight (kg) - "
           WITH NO ADVANCING 
           ACCEPT WEIGHT 
           DISPLAY   "Enter height (cm) - "
           WITH NO ADVANCING 
           ACCEPT HEIGHT  
           PERFORM CM-TO-M 
           PERFORM CAL-BMI
           EVALUATE TRUE 
             WHEN BMI < 18.50 SET LESS-WEIGHT TO TRUE 
             WHEN BMI >= 18.50 AND BMI < 23.00 SET NORMAL-WEIGHT TO TRUE 
             WHEN BMI >= 23.00 AND BMI < 25.00 SET BUXOM-WEIGHT TO TRUE 
             WHEN BMI >= 25.00 AND BMI < 30.00 SET FAT-WEIGHT TO TRUE 
             WHEN BMI >= 30.00 SET VERY-FAT-WEIGHT TO TRUE 
           END-EVALUATE 
           DISPLAY "BMI: " BMI 
           DISPLAY "DETAIL: " BMI-DETAIL 
           GOBACK 
           .
       CM-TO-M.
           COMPUTE HEIGHT = HEIGHT/100.
       CAL-BMI.
           COMPUTE BMI = WEIGHT / (HEIGHT **2).